from bitbucket_pipes_toolkit import Pipe
from googleapiclient import discovery
import json
import time
import urllib.request
from google.oauth2 import service_account
from google.cloud import storage

# defines the schema for pipe variables
variables = {
    'MODEL_CREDENTIAL_PATH': {'type': 'string', 'required': True},
    'STORAGE_CREDENTIAL_PATH': {'type': 'string', 'required': True},
    'MODEL_PROJECT_ID': {'type': 'string', 'required': True},
    'STORAGE_PROJECT_ID': {'type': 'string', 'required': True},
    'RUNTIME_VERSION': {'type': 'string', 'required': True},
    'PYTHON_VERSION': {'type': 'string', 'required': True},
    'MACHINE_TYPE': {'type': 'string', 'required': True},
    'MODEL_PATH': {'type': 'string', 'required': True},
    'CUSTOM_CODE_PATH': {'type': 'string', 'required': True},
    'CUSTOM_CODE_PATH2': {'type': 'string', 'required': True},
    'PREDICTION_CLASS': {'type': 'string', 'required': True},
    'MODEL_NAME': {'type': 'string', 'required': True},
    'MODEL_VERSION_NAME': {'type': 'string', 'required': True},
    'MIN_NODES_FOR_AUTOSCALING': {'type': 'integer', 'required': True},
    'ZIPPED_CUSTOM_CODE_FILE': {'type': 'string', 'required': True},
    'STORAGE_BUCKET_NAME': {'type': 'string', 'required': True},
    'CUSTOM_CODE_DESTINATION': {'type': 'string', 'required': True}
}

pipe = Pipe(schema=variables)
model_file_path = pipe.get_variable('MODEL_CREDENTIAL_PATH')
storage_file_path = pipe.get_variable('STORAGE_CREDENTIAL_PATH')
model_project_id = pipe.get_variable('MODEL_PROJECT_ID')
storage_project_id = pipe.get_variable('STORAGE_PROJECT_ID')
model_name = pipe.get_variable('MODEL_NAME')
model_path = pipe.get_variable('MODEL_PATH')
model_runtime_version = pipe.get_variable('RUNTIME_VERSION')
machine_type = pipe.get_variable('MACHINE_TYPE')
custom_code_path = pipe.get_variable('CUSTOM_CODE_PATH')
custom_code_path2 = pipe.get_variable('CUSTOM_CODE_PATH2')
python_version = pipe.get_variable('PYTHON_VERSION')
minimum_no_of_nodes = pipe.get_variable('MIN_NODES_FOR_AUTOSCALING')
prediction_class = pipe.get_variable('PREDICTION_CLASS')
model_version_name = pipe.get_variable('MODEL_VERSION_NAME')
zipped_file_path = pipe.get_variable('ZIPPED_CUSTOM_CODE_FILE')
storage_bucket_name = pipe.get_variable('STORAGE_BUCKET_NAME')
code_destination = pipe.get_variable('CUSTOM_CODE_DESTINATION')

# we should try to use only v1, we should delete the old version and recreate a new one with it

# initialize the Pipe object. At this stage the validation of variables takes place


request_dict = {
    "name": model_version_name,
    "description": "Current deployed model",
    "deploymentUri": model_path,
    "runtimeVersion": model_runtime_version,
    "machineType": machine_type,
    "packageUris": [
        custom_code_path, custom_code_path2
    ],
    "pythonVersion": python_version,
    "autoScaling": {
        "minNodes": minimum_no_of_nodes
    },
    "predictionClass": prediction_class
}


def getScopedCredentials(scopes, file_path):
    service_account_info = json.loads(file_path)
    credentials = service_account.Credentials.from_service_account_info(
        service_account_info)
    scoped_credentials = credentials.with_scopes(scopes)
    return scoped_credentials


def getMlClient():
    ml = discovery.build(serviceName='ml', version='v1',
                         credentials=getScopedCredentials(['https://www.googleapis.com/auth/cloud-platform'], model_file_path))
    return ml


def createModelVersion(ml=getMlClient()):
    try:
        pipe.log_info("Creating a new model version")
        response = ml.projects().models().versions().create(
            parent='projects/{}/models/{}'.format(model_project_id, model_name),
            body=request_dict).execute()
        if response['name'] != None:
            return True
    except Exception as e:
        pipe.log_error(f"Failed to create model version due to : {e}")
        return False

    return False


def deleteModelVersion(ml=getMlClient()):
    name = 'projects/{}/models/{}/versions/{}'.format(model_project_id, model_name, model_version_name)
    pipe.log_info("Deleting the model version")
    pipe.log_info(name)
    try:
        response = ml.projects().models().versions().delete(name=name).execute()
        if response['name'] != None:
            return True
    except Exception as e:
        print(f"Failed to delete old model version due to : {e}")
        return False

    return False


def deployNewModelVersion():
    ml = getMlClient()
    isCodeUploaded = uploadUpdatedCode()
    if not isCodeUploaded:
        return False
    isOldDeleted = deleteModelVersion(ml)
    time.sleep(30)
    isNewCreated = createModelVersion(ml)
    return isNewCreated


def uploadUpdatedCode():
    try:
        pipe.log_info("Uploading new code to the storage bucket")
        """Uploads a file to the bucket."""
        scopes = [
            "https://www.googleapis.com/auth/devstorage.full_control",
            "https://www.googleapis.com/auth/devstorage.read_only",
            "https://www.googleapis.com/auth/devstorage.read_write",
        ]
        storage_client = storage.Client(project=storage_project_id, credentials=getScopedCredentials(scopes, storage_file_path))
        bucket = storage_client.bucket(storage_bucket_name)
        blob = bucket.blob(code_destination)

        blob.upload_from_filename(zipped_file_path)

        pipe.log_info(
            "File {} uploaded to {}.".format(
                zipped_file_path, code_destination
            )
        )
        return True
    except Exception as e:
        pipe.log_error(f"Failed to upload updated code to google cloud storage due to: {e}")
    return False


pipe.log_info("Deploying new model version......")
isVersionDeployed = deployNewModelVersion()
if not isVersionDeployed:
    pipe.fail("Couldn't deploy new model version, error listed above")

pipe.success(message="Deployment successful!")